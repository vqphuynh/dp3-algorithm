#!/bin/bash
if [ "${1}" == "" ] || [ "${2}" == "" ]
then
  echo "Help: <node_count> <wall_time>"
  echo "Example: 3 3:00:00"
  exit
fi
echo "Requested node count: ${1}"
echo "Wall time: ${2}"
qsub -I -o ../stdout/info.out -j oe -l nodes=${1}:ppn=8,pmem=5888mb,walltime=${2} -v PATH,LD_LIBRARY_PATH -M vqphuynh@faw.jku.at
