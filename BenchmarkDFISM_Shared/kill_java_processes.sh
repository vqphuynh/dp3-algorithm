#!/bin/bash
echo "Kill java processes on nodes."
kill `ps | grep java | tr -s ' ' | cut -d' ' -f2`
oldNodeId=$(hostname)
for nodeId in $(<$PBS_NODEFILE)
do
  if [ "$nodeId" != "$oldNodeId" ]
  then
    oldNodeId=$nodeId
    ssh $nodeId kill `ps | grep java | tr -s ' ' | cut -d' ' -f2`
  fi
done
