package dp3.ms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import dfism.commons.Config;
import dfism.commons.Config.MiningModes;
import dfism.commons.GarbageCollector;
import dfism.commons.ItemSupport;
import dfism.commons.MemoryLogger;
import dfism.commons.SlaveInfo;
import dfism.fpo.FPOTree;
import dfism.threads.GlobalFrequentItemsSendingThread;
import dfism.threads.GlobalPotentialItemsSendingThread;
import dfism.threads.Local2ItemsetsFPOTreeReceivingThread;
import dfism.threads.LocalFPOTreeReceivingThread;
import dfism.threads.LocalFrequentItemsMergingThread;
import dfism.threads.LocalFrequentItemsReceivingThread;
import dfism.threads.LocalSupportCountOfPotentialItemsReceivingThread;
import dfism.threads.ParameterSendingThread;
import dfism.threads.SupportArrayReceivingThread;

public class Master {
	private static MemoryLogger memoryLogger = MemoryLogger.getInstance();
	
	private static ArrayList<SlaveInfo> slaves = new ArrayList<SlaveInfo>();
	
	private static int E_SLAVES_COUNT;
	private static int support_count_threshold;
	private static int TOTAL_TRANS_COUNT;
	private static int frequent_itemset_count;
	
	private static ArrayList<ItemSupport> global_frequent_items;
	private static String[] index_glbFrequentItems;
	
	public static void main(String[] args) throws Exception {
		System.out.println("Master's IP Address: " + InetAddress.getLocalHost().getHostAddress().trim());
		
		// Get running configuration
		Config.parse();
		
		// Get input parameters
		if(Config.is_auto_mode){
			System.out.println("Operate in the automatic mode.");
			if(Options.parse(args)) System.out.println(Options.getString());
			else return;
		}else {
			System.out.println("Operate in the interative mode.");
			if(Master.getParameterFromCommandLine()) System.out.println(Options.getString());
			else return;
		}
		
		// Get slave information
		ArrayList<String> input_filename_list = null;
		if(Config.is_distributed_file_system){
			System.out.println("Nodes use a global distributed file system for input datasets.");
			input_filename_list = new ArrayList<String>();
			File input_dir = new File(Config.input_data_directory);
			for(File file : input_dir.listFiles()){
				if(file.isDirectory()) continue;
				if(file.getName().contains(Options.filter_filename)) input_filename_list.add(file.getName());
			}
			E_SLAVES_COUNT = input_filename_list.size();
			
			// Initialize
			int try_count = 0;
			while(try_count < 6){
				try_count++;
				slaves.clear();
				Master.getSlavesInfo_dfs(Config.slave_address_directory, slaves);
				if(E_SLAVES_COUNT > slaves.size()){
					System.out.println("The expected slave count is not enough. Wait for 5 seconds then try again.");
					Thread.sleep(5000);
					continue;
				}else break;
			}
		} else{
			System.out.println("Nodes use their own local disks for input datasets.");
			Master.getSlavesInfo_localdisk(Config.slave_address_file, slaves);
			E_SLAVES_COUNT = slaves.size();
		}
		
		System.out.println("# Expected salves: " + E_SLAVES_COUNT);
		System.out.println("# Total of slaves: " + slaves.size());
		
		// Check the number of slaves is enough to start mining job
		if(E_SLAVES_COUNT > slaves.size()){
			Master.establishConnections(slaves);
			Master.sendParameters_toSlaves(slaves, MiningModes.EXIT.name());	// Shutdown all slaves
			System.out.println("The expected slave count is still not enough. Exit.");
			return;
		}else if (E_SLAVES_COUNT < slaves.size()){	// Fix the number of slaves = E_SLAVES_COUNT
			ArrayList<SlaveInfo> used_slaves = new ArrayList<SlaveInfo>(E_SLAVES_COUNT);
			ArrayList<SlaveInfo> unused_slaves = new ArrayList<SlaveInfo>(slaves.size() - E_SLAVES_COUNT);
			int i=0;
			for(; i<E_SLAVES_COUNT; i++) used_slaves.add(slaves.get(i));
			for(; i<slaves.size(); i++) unused_slaves.add(slaves.get(i));
			Master.establishConnections(unused_slaves);
			Master.sendParameters_toSlaves(unused_slaves, MiningModes.EXIT.name());	// Shutdown unused slaves
			slaves = used_slaves;
		}
		
		// Start mining job
		long start = System.currentTimeMillis();
		
		if(!Master.establishConnections(slaves)){
			System.out.println("The number of connections to active slaves is not enough for the mining job. Exit.");
			Master.sendParameters_toSlaves(slaves, MiningModes.EXIT.name());
			return;
		}
		
		System.out.println("------------------------------Master sends parameters to slaves------------------------------");
		String[] parameter_array = Master.buildParameterStringForSlaves(MiningModes.FROM_SCRATCH.name(), input_filename_list);
		if(Config.is_distributed_file_system){
			Master.sendParameters_toSlaves(slaves, parameter_array);
		}else{
			Master.sendParameters_toSlaves(slaves, parameter_array[0]);
		}
		
		if(cooperate_with_slaves()) System.out.println("Successful");
		
		System.out.println("Total count of frequent itemsets: " + Master.frequent_itemset_count);
		System.out.println("Total running time (ms): " + (System.currentTimeMillis() - start));
		System.out.println("Garbage Collector took time in (ms): " + GarbageCollector.getGarbageCollectionTime());
		System.out.println("Peak Memory (MB): " + memoryLogger.getMaxUsedMemory());
		System.out.println("===========================================FINISH============================================");
	}
	
	private static boolean getParameterFromCommandLine(){
		Options.printHelp();
		
		Scanner scanInput;
		while(true){
			System.out.print("Enter parameters: ");
			scanInput = new Scanner(System.in);
			
			if(Options.parse(scanInput.nextLine().trim().split(" "))){
				scanInput.close();
				return true;
			}else{
				System.out.println("Wrong parameters");
			}
			
			System.out.print("Continue? (y/n): ");
			if(scanInput.nextLine().trim().equalsIgnoreCase("n")){
				scanInput.close();
				return false;
			}
		}
	}
	
	/**
	 * Each slave when starting up will write down its ip address and port as the filename to the directory 'dir'
	 * </br> This procedure gets information from all slaves through all file names in the directory 'dir'
	 * @param dir
	 * @param slaves
	 */
	private static void getSlavesInfo_dfs(String dir, ArrayList<SlaveInfo> slaves){
		File slaveAddressDir = new File(dir);
		File[] listOfFiles = slaveAddressDir.listFiles();
	    for (int i = 0; i < listOfFiles.length; i++) {
	    	if (listOfFiles[i].isFile()){
	    		String[] info = listOfFiles[i].getName().split("_");
				SlaveInfo slaveInfo = new SlaveInfo(info[0], Integer.parseInt(info[1]));
				slaves.add(slaveInfo);
	    	}
	    }
	}
	
	/**
	 * In case nodes use their own local disks for partial dataset, the address and port of nodes must be aware in
	 * advance and configured in a file name 'filename'
	 * @param filename
	 * @param slaves
	 */
	private static void getSlavesInfo_localdisk(String filename, ArrayList<SlaveInfo> slaves){
		try {
			BufferedReader input = new BufferedReader(new FileReader(filename));
			String line;
			String[] info;
			while((line = input.readLine()) != null) {
				info = line.trim().split("_");
				if(info[0].charAt(0) == '#') continue;
				SlaveInfo slaveInfo = new SlaveInfo(info[0], Integer.parseInt(info[1]));
				slaves.add(slaveInfo);
			}
			input.close();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Establish and be sure that all connections to slaves are ready.
	 * @return true if all connections to all slaves are all ready.
	 */
	private static boolean establishConnections(List<SlaveInfo> slaves){
		boolean isReady = true;
		for(SlaveInfo slave : slaves){
			try {
				slave.socket = new Socket(slave.ip_address, slave.port);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				isReady = false;
			}				
		}
		return isReady;
	}
	
	/**
	 * Send the same parameters to slaves. Each thread sends to one slave.
	 * @param slaves
	 * @param parameter
	 * @return true if successful, otherwise failed
	 */
	private static boolean sendParameters_toSlaves(ArrayList<SlaveInfo> slaves, String parameter){
		try{
			int thread_count = slaves.size();
			Thread[] threads = new Thread[thread_count];
			for(int i=0; i<thread_count; i++){
				threads[i] = new ParameterSendingThread(i, slaves.get(i).socket, parameter);
				threads[i].start();
			}
			for(int i=0; i<threads.length; i++) threads[i].join();
			
			return true;
		}catch(Exception e){
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	/**
	 * Send the parameters to slaves. Each thread sends to one slave.
	 * @param slaves
	 * @param parameters
	 * @return true if successful, otherwise failed
	 */
	private static boolean sendParameters_toSlaves(ArrayList<SlaveInfo> slaves, String[] parameters){
		try{
			int thread_count = slaves.size();
			Thread[] threads = new Thread[thread_count];
			for(int i=0; i<thread_count; i++){
				threads[i] = new ParameterSendingThread(i, slaves.get(i).socket, parameters[i]);
				threads[i].start();
			}
			for(int i=0; i<threads.length; i++) threads[i].join();
			
			return true;
		}catch(Exception e){
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	private static String[] buildParameterStringForSlaves(String mining_mode, ArrayList<String> input_filename_list){
		StringBuilder sb = new StringBuilder(100);
		
		if(Config.is_distributed_file_system){
			String[] parameter_array = new String[input_filename_list.size()];
			int index=0;
			for(String filename : input_filename_list){
				parameter_array[index] = sb.append(mining_mode).append(' ').append(filename).append(' ')
										.append(Options.support_threshold).append(' ').append(Options.part_count).toString();
				index++;
				sb.setLength(0);
			}
			return parameter_array;
		}else{
			sb.append(mining_mode).append(' ').append(Options.filter_filename).append(' ')
				.append(Options.support_threshold).append(' ').append(Options.part_count);
			return new String[]{sb.toString()};
		}
	}
	
	/**
	 * Close all connection to slaves
	 * @param slaves
	 */
	@SuppressWarnings("unused")
	private static void closeConnections(List<SlaveInfo> slaves){
		try{
			for(SlaveInfo slave : slaves){
				if(slave.socket != null && slave.socket.isConnected()){
					slave.socket.close();
				}
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	private static boolean cooperate_with_slaves(){
		try {
			Master.achieve_globalFrequentItems();	// Achieve the global frequent items
			if(Options.part_count < 2){
				Master.achieve_globalFrequentItemsets();
			}else{
				System.out.println("Write global frequent items in (ms): " + Master.write_global_frequent_items());
				Master.achieve_globalFrequentItemsets_PARTS();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	private static void achieve_globalFrequentItems() throws InterruptedException, IOException{
		System.out.println("-------------------Master receives and merges the local frequent item lists------------------");
		ArrayList<String> global_potential_items = Master.receive_merge_localFrequentItems();
		System.out.println("Total transaction count: " + TOTAL_TRANS_COUNT);
		System.out.println("Support count threshold: " + support_count_threshold);
		
		System.out.println("-------------------------Master sends global potential items to slaves-----------------------");
		System.out.println("Master sends global potential items to slaves (ms): " + 
				Master.send_globalPotentialItems(global_potential_items));
		
		System.out.println("------Master receives and acumulates the local support counts of global potential items------");
		Master.global_frequent_items = Master.receive_accumulate_supportCounts_globalPotentialItems(global_potential_items);
		
		System.out.println("-------------------------Master sends global frequent items to slaves------------------------");
		System.out.println("Master sends global potential items to slaves (ms): " + 
				Master.send_globalFrequentItems(Master.global_frequent_items));
		
		// Fill index_glbFrequentItems
		int size = global_frequent_items.size();
		Master.index_glbFrequentItems = new String[size];
		for(int i=0; i<size; i++) index_glbFrequentItems[i] = global_frequent_items.get(i).item;
	}
	
	private static ArrayList<String> receive_merge_localFrequentItems() throws InterruptedException{
		long start = System.currentTimeMillis();
		
		int[] localTransCount = new int[E_SLAVES_COUNT];
		ArrayList<ArrayList<String>> local_frequent_item_arrays = new ArrayList<ArrayList<String>>(E_SLAVES_COUNT);
		
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new LocalFrequentItemsReceivingThread(i, slaves.get(i).socket, 
																localTransCount, 
																local_frequent_item_arrays);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		System.out.println("Master waited and received ALL local frequent item lists in (ms): " + (System.currentTimeMillis()-start));
		
		start = System.currentTimeMillis();
		
		// Summarize local transaction counts
		TOTAL_TRANS_COUNT = 0;
		for(int count : localTransCount) TOTAL_TRANS_COUNT += count;
		support_count_threshold = (int) (TOTAL_TRANS_COUNT*Options.support_threshold);
		
		// Prepare job queue for Parallel merging
		LinkedList<Integer> jobQueue = new LinkedList<Integer>();
		for(int i=0; i<local_frequent_item_arrays.size(); i++) jobQueue.add(i);
		
		// Determine the thread count for parallel merging
		int coreNum = Runtime.getRuntime().availableProcessors();
		int threadNum = (coreNum <= local_frequent_item_arrays.size()/2)? coreNum : local_frequent_item_arrays.size()/2;
		
		// Merge parallel
		threads = new Thread[threadNum];
		for(int i=0; i<threadNum; i++){
			threads[i] = new LocalFrequentItemsMergingThread(local_frequent_item_arrays, jobQueue, i);
			threads[i].start();
		}
		for(int i=0; i<threadNum; i++) threads[i].join();
		
		System.out.println("Master merged ALL local frequent item lists in (ms): " + (System.currentTimeMillis()-start));
		
		// After finishing, the jobQueue contains only the ID of the totally merged local frequent items
		// Get the totally merged items list
		return local_frequent_item_arrays.get(jobQueue.getFirst());
	}
	
	private static long send_globalPotentialItems(ArrayList<String> global_potential_items) throws InterruptedException{
		long start = System.currentTimeMillis();
		
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new GlobalPotentialItemsSendingThread(i, slaves.get(i).socket, global_potential_items);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		
		return System.currentTimeMillis()-start;
	}
	
	private static ArrayList<ItemSupport> receive_accumulate_supportCounts_globalPotentialItems(ArrayList<String> global_potential_items) 
	throws InterruptedException{
		long start = System.currentTimeMillis();
		
		int ITEM_COUNT = global_potential_items.size();
		int[][] local_supportCounts_arrays = new int[E_SLAVES_COUNT][ITEM_COUNT];
		
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new LocalSupportCountOfPotentialItemsReceivingThread(i, slaves.get(i).socket, 
																				local_supportCounts_arrays[i]);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		System.out.println("Master waited and received ALL local support counts of global potential item in (ms): " + (System.currentTimeMillis()-start));
		
		start = System.currentTimeMillis();
		
		// Accumulate support counts for global potential items
		for(int j=0; j<ITEM_COUNT; j++)
			for(int i=1; i<local_supportCounts_arrays.length; i++)
				local_supportCounts_arrays[0][j] += local_supportCounts_arrays[i][j];
		int[] sum_supports = local_supportCounts_arrays[0];
		
		// Sort increasingly frequency-based
		ArrayList<ItemSupport> global_frequent_items = new ArrayList<ItemSupport>(ITEM_COUNT);
		for(int i=0; i<ITEM_COUNT; i++){
			if(sum_supports[i]<support_count_threshold) continue;
			global_frequent_items.add(new ItemSupport(global_potential_items.get(i), sum_supports[i]));
		}
		Collections.sort(global_frequent_items);
		
		System.out.println("Master prepare global frequent items list in increasing order of frequency in (ms): " + (System.currentTimeMillis()-start));
		
		return global_frequent_items;
	}
	
	private static long send_globalFrequentItems(ArrayList<ItemSupport> global_frequent_items) throws InterruptedException{
		long start = System.currentTimeMillis();
		
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new GlobalFrequentItemsSendingThread(i, slaves.get(i).socket, global_frequent_items);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		
		return System.currentTimeMillis()-start;
	}
	
	private static long write_global_frequent_items() throws IOException{
		long start = System.currentTimeMillis();
		String result_filename = Config.output_data_directory + Options.filter_filename + 
								("_" + Options.support_threshold + "_items").replace(".", "");
		
		BufferedWriter output = new BufferedWriter(new FileWriter(result_filename));
		StringBuilder sb = new StringBuilder();
		for(ItemSupport itemSupport : global_frequent_items){
			output.write(sb.append(itemSupport.item).append(':').append(itemSupport.support).append('\n').toString());
			sb.setLength(0);
		}
		output.flush();
		output.close();
		Master.frequent_itemset_count = global_frequent_items.size();
		
		return System.currentTimeMillis()-start;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	private static void achieve_globalFrequentItemsets() throws IOException, InterruptedException{
		System.out.println("-----------------------Master receives and merges the local FPM trees------------------------");
		FPOTree fpm_tree = Master.receive_merge_localFPMTrees();	// fpm_tree is now the total merged FPM-Tree
		System.out.println("Time for counting all kinds of nodes in (ms): " + fpm_tree.count_nodes());
		memoryLogger.checkMemory();
		
		System.out.println("-----------------------Master sends the total merged FPM-Tree to slaves----------------------");
		System.out.println("The total sending time (ms): " + fpm_tree.send_only_itemCode_toSlaves(slaves));
		
		System.out.println("-------------------------The information of total mergered FPM tree--------------------------");
		System.out.println("Total node count: " + fpm_tree.getNodeCount());
		System.out.println("\tInner node count: " + fpm_tree.getInnerNodeCount());
		System.out.println("\tLeaf node count: " + fpm_tree.getLeafNodeCount());
		
		System.out.println("-------------------------Master receive and accumulate support counts------------------------");
		Master.receive_accumulate_supportCount(fpm_tree);
		
		// Write result
		String result_filename = Config.output_data_directory + Options.filter_filename + 
								("_" + Options.support_threshold).replace(".", "");
		System.out.println("Result writing time in (ms): " + 
				fpm_tree.filter_write_frequentPatterns(index_glbFrequentItems,
														result_filename,
														support_count_threshold));
		Master.frequent_itemset_count = fpm_tree.getNodeCount();
		
		System.out.println("---------------------------------------------------------------------------------------------");
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	private static void achieve_globalFrequentItemsets_PARTS() throws IOException, InterruptedException{
		System.out.println("-------------Master receives and merges the FPM trees of local frequent 2-itemset------------");
		FPOTree fpm_tree = Master.receive_merge_localFPMTrees_of_2itemsets(); // fpm_tree is now the total merged FPM-Tree
		System.out.println("Time for counting all kinds of nodes in (ms): " + fpm_tree.count_nodes());
		System.out.println("=> Count of potential global 2-itemsets: " + fpm_tree.getLeafNodeCount());
		
		System.out.println("---------Master sends the total merged FPM-Tree of local frequent 2-itemset to slaves--------");
		System.out.println("The total sending time (ms): " + fpm_tree.send_only_itemCode_toSlaves(slaves));		
		fpm_tree.free(); fpm_tree = null;	// The fpm_tree is no longer used
		
		System.out.println();
		System.out.println("-------------------------LOOP of Achieving Global Frequent Itemsets--------------------------");
		String result_filename = Config.output_data_directory + Options.filter_filename + 
								("_" + Options.support_threshold + "_part_").replace(".", "");
		for(int i=0; i<Options.part_count; i++){
			long start = System.currentTimeMillis();
			System.out.println("-------------------------------------------PART " + i + "--------------------------------------------");
			System.out.println("GC time in (ms): " + GarbageCollector.collectMemory());
			Master.frequent_itemset_count += Master.achieve_globalFrequentItemsets_ONEPART(result_filename + i);
			System.out.println("Time for this part in (ms): " + (System.currentTimeMillis()-start));
		}
		System.out.println("---------------------------------------------------------------------------------------------");
	}
	
	/**
	 * Receive local FPM trees of 2-itemsets from slaves and then return the total merged FPM trees.
	 * @return The total merged FPM trees
	 * @throws InterruptedException
	 */
	private static FPOTree receive_merge_localFPMTrees_of_2itemsets() throws InterruptedException{
		long start = System.currentTimeMillis();
		
		FPOTree[] fpm_trees = new FPOTree[E_SLAVES_COUNT];
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new Local2ItemsetsFPOTreeReceivingThread(i, slaves.get(i).socket, fpm_trees);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		System.out.println("Master waited and received ALL local FPM trees of 2-itemsets in (ms): " + (System.currentTimeMillis()-start));
		
		start = System.currentTimeMillis();
		
		// Merge parallel
		FPOTree total_tree = fpm_trees[0];
		for(int i=1; i<fpm_trees.length; i++) {
			System.out.println("Merge two local FPM trees in (ms): " + total_tree.merge(fpm_trees[i]));
		}
		
		System.out.println("Master merged ALL local FPM trees in (ms): " + (System.currentTimeMillis()-start));
		
		return total_tree;
	}
	
	private static int achieve_globalFrequentItemsets_ONEPART(String result_filename) throws IOException, InterruptedException{
		System.out.println("-----------------------Master receives and merges the local FPM trees------------------------");
		FPOTree fpm_tree = Master.receive_merge_localFPMTrees();	// fpm_tree is now the total merged FPM-Tree
		System.out.println("Time for counting all kinds of nodes in (ms): " + fpm_tree.count_nodes());
		memoryLogger.checkMemory();
		
		System.out.println("-----------------------Master sends the total merged FPM-Tree to slaves----------------------");
		System.out.println("The total sending time (ms): " + fpm_tree.send_only_itemCode_toSlaves(slaves));
		
		System.out.println("-------------------------The information of total mergered FPM tree--------------------------");
		System.out.println("Total node count: " + fpm_tree.getNodeCount());
		System.out.println("\tInner node count: " + fpm_tree.getInnerNodeCount());
		System.out.println("\tLeaf node count: " + fpm_tree.getLeafNodeCount());
		
		System.out.println("-------------------------Master receive and accumulate support counts------------------------");
		Master.receive_accumulate_supportCount(fpm_tree);
		
		// Write result
		System.out.println("Result writing time in (ms): " + 
				fpm_tree.filter_write_frequentPatterns_omitL1(index_glbFrequentItems,
															result_filename,
															support_count_threshold));
		System.out.println("The number of frequent itemsets (omit 1-itemset): " + fpm_tree.getNodeCount());
		
		return fpm_tree.getNodeCount();
	}
	
	/**
	 * Receive local FPM trees from slaves and then return the total merged FPM trees.
	 * @return The total merged FPM trees
	 * @throws InterruptedException
	 */
	private static FPOTree receive_merge_localFPMTrees() throws InterruptedException{
		long start = System.currentTimeMillis();
		
		FPOTree[] fpm_trees = new FPOTree[E_SLAVES_COUNT];
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new LocalFPOTreeReceivingThread(i, slaves.get(i).socket, fpm_trees, null);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		System.out.println("Master waited and received ALL local FPM trees in (ms): " + (System.currentTimeMillis()-start));
		
		start = System.currentTimeMillis();
		
		// Merge parallel
		FPOTree total_tree = fpm_trees[0];
		for(int i=1; i<fpm_trees.length; i++) {
			System.out.println("Merge two local FPM trees in (ms): " + total_tree.merge(fpm_trees[i]));
		}
		
		System.out.println("Master merged ALL local FPM trees in (ms): " + (System.currentTimeMillis()-start));
		
		return total_tree;
	}
	
	/**
	 * Receive an array of support counts from each slave, then a summarization is performed.
	 * These summarized support counts will then be accumulated into the support counts of
	 * nodes belonging to the upper part of the total FPM-Tree. 
	 * <br/> Note: The order of nodes' support counts from all slaves are the same as the order of nodes in the upper part tree.
	 * @return running time, -1 if failed
	 * @throws InterruptedException
	 */
	private static void receive_accumulate_supportCount(FPOTree fpm_tree) throws InterruptedException{
		long start = System.currentTimeMillis();
		
		int[][] supportArrays = new int[E_SLAVES_COUNT][fpm_tree.getNodeCount()];
		
		Thread[] threads = new Thread[E_SLAVES_COUNT];
		for(int i=0; i<E_SLAVES_COUNT; i++){
			threads[i] = new SupportArrayReceivingThread(i, slaves.get(i).socket, supportArrays[i]);
			threads[i].start();
		}
		for(int i=0; i<threads.length; i++) threads[i].join();
		
		// Summarize arrays of supports into the 'supportArrays[0]'
		int[] supports = supportArrays[0];
		for(int j=0; j<supports.length; j++){
			for(int i=1; i<supportArrays.length; i++){
				supports[j] = supports[j] + supportArrays[i][j];
			}
		}
		
		System.out.println("Master waited and received the total support array in (ms): " + (System.currentTimeMillis()-start));
		
		// Accumulate support counts in array 'supports' to the total FPMTree
		System.out.println("Master accumulated support counts in (ms): " + fpm_tree.accumulate_supportCount(supports));
	}
}
