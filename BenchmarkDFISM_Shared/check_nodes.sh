#!/bin/bash
echo "----------------------------------------------------------------"
oldNodeId=$(hostname)
echo "NODE: $oldNodeId"
    echo -n "<Total Memory> <Free Memory>: "
    free -h | grep Mem: | tr -s ' ' | cut -d' ' -f2,4; ps | grep java
echo

for nodeId in $(<$PBS_NODEFILE)
do
  if [ "$nodeId" != "$oldNodeId" ]
  then
    oldNodeId=$nodeId
    echo "NODE: $nodeId"
    echo -n "<Total Memory> <Free Memory>: "
    ssh $nodeId "free -h | grep Mem: | tr -s ' ' | cut -d' ' -f2,4; ps | grep java"
    echo
  fi
done
