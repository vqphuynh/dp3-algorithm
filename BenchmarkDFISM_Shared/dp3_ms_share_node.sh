#!/bin/bash
if [ "${1}" == "" ] || [ "${2}" == "" ] || [ "${3}" == "" ]
then
  echo "Help"
  echo "Parameters: <filter_filename> <support_threshold> <part_count>"
  echo "<filter_filename> is a subtring of input files in the input directory which help master filter the input files."
  echo "<part_count> is the number of parts of all global potential 2-itemsets is divided."
  echo "<part_count> often is 1. Depend on available memory and support threshold that this value is bigger"
  echo "Example for slaves of 36GB to 44GB memory free for Java-Heap: webdocs 0.05 6"
  exit
fi
filter_filename=${1}
support_threshold=${2}
part_count=${3}
timestamp=$(date +%Y%m%d_%k_%M_%S)

echo "============================================================================================================="
echo "Start script ..."
export JAVA_HOME=/apps/jdk1.8.0_162
export PATH=${PATH}:${JAVA_HOME}/bin
echo "JAVA_HOME = $JAVA_HOME"
echo "PATH = $PATH"
echo "Empty directory slave_address"
rm ./slave_address/*
echo "Empty directory stdout"
rm ./stdout/*

num_of_nodes=0
echo "List of nodes:"
for nodeId in $(<$PBS_NODEFILE)
do
  if [ "$nodeId" != "$oldNodeId" ]
  then
    oldNodeId=$nodeId
    echo $nodeId
    num_of_nodes=$(($num_of_nodes+1))
  fi
done

echo "The number of nodes is $num_of_nodes."

# Record memory status of nodes
mem_monitor_index=0
./check_nodes.sh > ./stdout/mem_status_${mem_monitor_index}.out

echo "-------------------------------------------------MASTER------------------------------------------------------"
oldNodeId=$(hostname)
IPAddress=$(hostname --ip-address)
nohup java -Xms6G -Xmx6G -cp ./dfism.jar dp3.ms.Master -ff ${filter_filename} -st ${support_threshold} -pc ${part_count} > ./stdout/${timestamp}_master.out &
echo "Master started at: nodeId=$oldNodeId, IP=$IPAddress"

# Get ID of the master process running in background
MASTER_PID=$!
echo "Master Process ID: ${MASTER_PID}"

echo "-------------------------------------------------SLAVES------------------------------------------------------"

echo "-------------------------------The slave on the same machine with the master---------------------------------"
i=0
nohup java -Xms38G -Xmx38G -cp ./dfism.jar dp3.ms.Slave > ./stdout/${timestamp}_slave${i}.out &
echo "Slave$i is waiting at: nodeId=$oldNodeId"
echo "Slave${i}'s IP address: $IPAddress"
i=$(($i+1))

echo "-----------------------------------------------Other Slaves--------------------------------------------------"
for nodeId in $(<$PBS_NODEFILE)
do
  if [ "$nodeId" != "$oldNodeId" ]
  then
    oldNodeId=$nodeId
    ssh $nodeId << EOF
echo "Change to directory: $PWD";
cd $PWD;
module load java;
nohup java -Xms44G -Xmx44G -cp ./dfism.jar dp3.ms.Slave > ./stdout/${timestamp}_slave${i}.out &;
echo "------------------------------------------------------";
echo "Slave$i is waiting at: nodeId=$nodeId";
echo -n "Slave${i}'s IP address: ";
hostname --ip-address;
echo "-------------------------------------------------------------------------------------------------------------"
EOF
    i=$(($i+1))
  fi
done
echo "The total number of running slaves: $i"

# Monitor the master process status and keep connection
echo ""
while :
do
  status=`ps | grep ${MASTER_PID}`
  if [ "${status}" != "" ]
  then
    echo "MASTER_PROCESS-${MASTER_PID} is running."
    mem_monitor_index=$((${mem_monitor_index}+1))
    ./check_nodes.sh > ./stdout/mem_status_${mem_monitor_index}.out
    sleep 30
  else
    echo "Completed."
    break;
  fi
done

#Notify the completion by sound
while :
do
  echo -ne '\007'
  sleep 1
done

